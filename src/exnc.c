/**
 * Conjoined .h/.c of the exnc utility
 */

#include <sys/socket.h>         /* socket */
#include <sys/select.h>         /* select */
#include <stdio.h>              /* fprintf, snprintf, fwrite */
#include <getopt.h>             /* getopt_long */
#include <string.h>             /* strlen, strncpy */
#include <stdlib.h>             /* EXIT_SUCCESS, EXIT_FAILURE, strtoul */
#include <errno.h>              /* errno */
#include <unistd.h>             /* read */
#include <arpa/inet.h>          /* htons */

/**
 * Function prototypes
 */
int usage(char *);
int version(void);
int getopt_err(int err_id, char * err_msg);
int hex_translate(char dir, unsigned char * input,
                unsigned char * output, signed long len,
                signed long * parse_len);
int readFromAddr(char * host_addr, unsigned short port,
    unsigned char * rcvd_str, signed long * read_len, signed long max_read);
int writeToAddr(char * host_addr, unsigned short port,
                unsigned char * txcv_str, signed long len);


#define EXIT_GOPT 2

#define PROG_NAME "EXNC (Echo Xxd NCat)"
#define VERSION_ID "0.9.7"

#define PROG_PERR "Program parameter error: "

#define MODE_UNDECIDED (unsigned char) 0x5a
#define MODE_LISTEN (unsigned char) 0xfa
#define MODE_SEND (unsigned char) 0x5f
#define ADDR_C_LEN 15
#define PORT_C_LEN 5

int usage(char * prog_name)
{
    fprintf(stdout, PROG_NAME" "VERSION_ID"\n"); 
    fprintf(stdout, "Usage: %s [-hvx] [-s | -l] host port\n", 
            prog_name);
    fprintf(stdout, "\t-x, --hex\t- hex mode\n");
    fprintf(stdout, "\t-s, --send\t- send a packet\n");
    fprintf(stdout, "\t-l, --listen\t- listen for a packet\n");
    fprintf(stdout, "\thost\t\t- the host sent to or received from\n");
    fprintf(stdout, "\tport\t\t- the port on the host sent to or received "
            "from\n");
    fprintf(stdout, "\n"
            "\t-h, --help\t- displays help message\n");
    fprintf(stdout, "\t-v, --version\t- the message sent to host\n");
    fprintf(stdout, "\n"
            "Sends or receives a single UDP packet with binary data. Transmitted"
            "\nand received messages are hex-encoded strings. Hex mode converts"
            "\nhex to binary before transmission or binary to hex on reception."
            "\n");
    return EXIT_SUCCESS;
}

int version()
{
    fprintf(stdout, PROG_NAME" "VERSION_ID"\n");
    return EXIT_SUCCESS;
}

int getopt_err(int err_id, char * err_msg)
{
    fprintf(stderr, PROG_PERR"%s", err_msg);
    exit(err_id);
}

int hex_translate(char dir, unsigned char * input,
                unsigned char * output, signed long len,
                signed long * parse_len) {
    if(0 == dir) {
        memcpy(output, input, len); // OPTIMIZATION: instead, modify output to
                                    // point to input
        *parse_len = len;
        return EXIT_SUCCESS;
    }
    unsigned long i = 0;
    unsigned long lim = len;
    int rc;
    if(dir & 0x01) {            // if odd, send (translate from hex to bin)
    // len is the number of bytes in input
        unsigned char tmp[2] = {0};
        if(lim & 0x01) {
            lim -= 1;
        }
        for(; i < lim && *(input+i) !='\0'; i++) {
            if(i%2) {
                continue;
            }
            if(2 != (rc = sscanf((char *) (input + i), "%1hhx %1hhx", tmp, tmp+1))) {
                if(i == lim - 2 && 1 == rc) {
                    *(tmp + 1) = *tmp;
                    *tmp = 0;
                }
                else {
                    fprintf(stderr, "Hex-to-binary translation failure.\n");
                    return EXIT_FAILURE;
                }
            }
            output[i/2] = (unsigned char)((tmp[0] << 4) | tmp[1]);
        }
        i /= 2;
    }
    else {                      // if even, receive (translate from bin to hex)
    // we expect to write len*2 bytes to output
        for(; i < lim; i++) {
            if(2 != sprintf((char *) (output + 2*i), "%02hhx", *(input + i))) {
                fprintf(stderr, "Binary-to-hex translation failure.\n");
                return EXIT_FAILURE;
            }
        }
        i *= 2;
    }    
    *parse_len = i;
    return EXIT_SUCCESS;
} 

int readFromAddr(char * host_addr, unsigned short port,
    unsigned char * rcvd_str, signed long * read_len, signed long max_read)
{
    struct sockaddr_in rcvd_sa;
    rcvd_sa.sin_family = AF_INET;
    if(0 == inet_aton(host_addr, &(rcvd_sa.sin_addr))) {
        fprintf(stderr, "Invalid host address.\n");
        return EXIT_GOPT;
    }
    rcvd_sa.sin_port = htons(port);
    int fd;
    if(0 > (fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))) {
        fprintf(stderr, "Unable to create socket (%s: %hu).\n", host_addr, port);
        return EXIT_FAILURE;
    }
    if(0 > (bind(fd, (const struct sockaddr *) &rcvd_sa, sizeof(rcvd_sa)))) {
        close(fd);
        fprintf(stderr, "Unable to bind socket (%s:%hu).\n", host_addr, port);
        return EXIT_FAILURE;
    }
    fd_set rset;
    FD_ZERO(&rset);
    FD_SET(fd, &rset);
    int sel_stat = select(fd + 1, &rset, NULL, NULL, NULL);
    if(0 > sel_stat) {
        close(fd);
        fprintf(stderr, "Error waiting for datagram.\n");
        return EXIT_FAILURE;
    }
    else if(0 == sel_stat) {
        close(fd);
        fprintf(stderr, "No message received.\n");
        return EXIT_SUCCESS;
    }
    if(-1 == (*read_len = recvfrom(fd, rcvd_str, max_read, 0, NULL, NULL))) {
        fprintf(stderr, "Error reading from socket.\n");
        close(fd);
        return EXIT_FAILURE;
    }
    close(fd);
    return EXIT_SUCCESS;
}

int writeToAddr(char * host_addr, unsigned short port,
                unsigned char * txcv_str, signed long len)
{
    struct sockaddr_in txcv_sa;
    txcv_sa.sin_family = AF_INET;
    if(0 == inet_aton(host_addr, &(txcv_sa.sin_addr))) {
        fprintf(stderr, "Invalid host address.\n");
        return EXIT_GOPT;
    }
    txcv_sa.sin_port = htons(port);
    int fd;
    if(0 > (fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))) {
        fprintf(stderr, "Unable to create socket (%s: %hu).\n", host_addr, port);
        return EXIT_FAILURE;
    }
    if(-1 == sendto(fd, txcv_str, len, 0, (const struct sockaddr *) &txcv_sa,
                                                sizeof(txcv_sa))) {
        close(fd);
        fprintf(stderr, "Unable to create socket (%s: %hu).\n", host_addr, port);
        return EXIT_FAILURE;
    }
    close(fd);
    return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
    int nopt;
    const char * const shopt = "hvxsl";
    const struct option longopt[] = {
        { "help", 0, NULL, 'h' },
        { "version", 0, NULL, 'v' },
        { "hex", 0, NULL, 'x' },
        { "send", 0, NULL, 's' },
        { "listen", 0, NULL, 'l' },
        { NULL, 0, NULL, 0 }
    };
   
    unsigned char op_mode = MODE_UNDECIDED;
    unsigned char hex_mode = 0x00;

    char host_addr[ADDR_C_LEN + 1]; // IPv4 allows for up to 11 character
    unsigned short host_port = 0;
    
    while(1) {
        nopt = getopt_long(argc, argv, shopt, longopt, NULL);
        if(-1 == nopt)
        {
            break;
        }
        switch(nopt) {
            case 'h':
                return usage(argv[0]);
            case 'v':
                return version();
            case 's':
                if(MODE_UNDECIDED == op_mode) {
                    op_mode = MODE_SEND;
                } else {
			        getopt_err(1, "Cannot set mutually exclusive modes: "
				        "send, listen.\n");
		        }
                break;
            case 'l':
                if(MODE_UNDECIDED == op_mode) {
                    op_mode = MODE_LISTEN;
                } else {
			        getopt_err(1, "Cannot set mutually exclusive modes: "
				        "send, listen.\n");
		        }
                break;
            case 'x':
                hex_mode = 0xff;
                break;

            case '?':
                break;

            default:
                fprintf(stderr, "\n"PROG_PERR"Getop returned code 0%o.\n", nopt);
                return EXIT_GOPT;
        }
    }
    if (optind < argc) {
            if(ADDR_C_LEN < strlen(argv[optind])) {
                getopt_err(1, "Invalid host address.\n");
            }
            if(host_addr != 
                    strncpy(host_addr, argv[optind], ADDR_C_LEN+1)) {
                getopt_err(1, "Unable to process host address.\n");
            }
    }
    else {
        usage(argv[0]);
        getopt_err(1, "Missing mandatory arguments.\n");
    }
    optind++;
    if (optind < argc) {
        errno = 0;
        char * errstr_ptr;
        unsigned long prosp_port =
                    strtoul(argv[optind], &errstr_ptr, 10);
        if(ERANGE == errno) {
            fprintf(stderr, PROG_PERR"Port value out of range: %s.\n", 
                        argv[optind]);
            return EXIT_GOPT;
        }
        if(prosp_port >= (1 << 16)) {
            fprintf(stderr, PROG_PERR"Port value out of range: %s.\n", 
                        argv[optind]);
            return EXIT_GOPT;
        }
        if('\0' == *argv[optind] || '\0' != *errstr_ptr) {
            fprintf(stderr, PROG_PERR"Error interpreting value: %s.\n", 
                        argv[optind]);
            return EXIT_GOPT;
        }
        host_port = (unsigned short) prosp_port;
        optind++;
    }
    else {
        usage(argv[0]);
        getopt_err(1, "Missing mandatory argument.\n");
    }
    if(MODE_UNDECIDED == op_mode) {
        return usage(argv[0]);
    }
    unsigned char read_buf[(1<<16) - 1];
    unsigned char proc_buf[(1<<16) - 1];
    signed long len;
    if(op_mode == MODE_SEND) {
        len = read(0, read_buf, (1<<16) -1);
        if(0 > len) {
            fprintf(stderr, "Unable to read from stdin.\n");
            return EXIT_FAILURE;
        }
        if(hex_translate((char)(op_mode * hex_mode), read_buf, proc_buf
                        , len, &len)) {
            return EXIT_FAILURE;
        }
        if(writeToAddr(host_addr, host_port, proc_buf, len)) {
            return EXIT_FAILURE;
        }
    }
    else if(op_mode == MODE_LISTEN) {
        if(readFromAddr(host_addr, host_port, read_buf, &len, (1<<16) - 1)) {
            return EXIT_FAILURE;
        }
        if(hex_translate((char)(op_mode * hex_mode),  read_buf,
                         proc_buf, len, &len)) {
            return EXIT_FAILURE;
        }
        if(len > write(1, proc_buf, len)) {
            fprintf(stderr, "Error reporting results.\n");
            return EXIT_FAILURE;
        }
        fflush(stdout);
    }
    else {
        fprintf(stderr, "No operation defined.\n");
        return EXIT_FAILURE;
    }
}

// TODO: make error logging a #define
